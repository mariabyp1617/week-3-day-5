@extends('adminlte.master')

@section('content')

<div class="card">
    <div class="card-header">
      <h3 class="card-title">List Pertanyaan</h3>
        @csrf
      <div class="card-tools" method="GET">
        <div class="input-group input-group-sm" style="width: 150px;">
            <div class="btn-group" role="group" aria-label="Basic example">
                <a href="/pertanyaan/create"><button >Create</button> </a>
            </div>
        </div>
      </div>
    </div>
        
    <!-- /.card-header -->
    <div class="card-body table-responsive p-0">
      <table class="table table-hover text-nowrap">
        <thead>
          <tr>
            <th>Title</th>
            <th>contents</th>
          </tr>
        </thead>
        @foreach($isi ad $key => $isi)
            <tr>
                <td>{{$key+1}}</td>
                <td>{{$isi->judul}}</td>
                <td>{{$isi->isi}}</td>  
                <td>
                    <a href="/pertanyaan/{{$isi->id}}" class="btn btn-info btn-sm">show</a>
                </td>   
            </tr>    
        @endforeach
        <tbody>
          <tr>
            <td><span class="tag tag-success">Laravel</span></td>
            <td>Bacon ipsum dolor sit amet salami venison chicken flank fatback doner.</td>
          </tr>
          <tr>

        </tbody>
      </table>
    </div>
    <!-- /.card-body -->
  </div>


@endsection