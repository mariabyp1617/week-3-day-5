@extends('adminlte.master')

@section('content')

<div class="card card-warning">
    <div class="card-header">
      <h3 class="card-title">Create Question</h3>
    </div>
    <!-- /.card-header -->
    <div class="card-body">
      <form role="form" action="/pertanyaan" method="POST">
        @csrf
        <!-- Title -->
        <div class="row">
          <div class="col-sm-6">
            
            <div class="form-group">
              <label for="title">Title</label>
              <input type="text" name="title" class="form-control"  placeholder="Enter title">
            </div>
          </div>
        
        </div>

        <!-- Question -->
        <div class="row">
          <div class="col-sm-6">
            <div class="form-group">
              <label for="question">Question</label>
              <textarea class="form-control" name="question" rows="3" placeholder="Enter question"></textarea>
            </div>
          </div>
        </div>

        <div class="card-footer">
            <button type="submit" class="btn btn-primary">Submit</button>
        </div>
      </form>
    </div>
    <!-- /.card-body -->
  </div>

@endsection