<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
class PertanyaanController extends Controller
{
    public function index()
    {
        $isi = DB::table('pertanyaan')->get();
        return view('pertanyaan.index', compact('isi'));
    }
    public function create()
    {
        return view('pertanyaan.create');
    }
    public function store(Request $request)
    {
        //dd($request->all());
        $query = DB::table('pertanyaan')->insert([
            "judul" => $request["title"],
            "isi" => $request["question"]
        ]);

        return redirect('/pertanyaan');
    }
    public function show($pertanyaan_id)
    {
        $isi = DB::table(pertanyaan)->where('$pertanyaan_id', $id)->first();
        return view('pertanyaan.show', compact('isi'));
    }
}
